const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const mongoose=require('mongoose');

const distSchema = new mongoose.Schema({
    distName:{
        type:String,
        maxlength: 255,
        minlength:3,
        required:true
    },
    distEmail:{
        type:String,
        maxlength:20,
        minlength:10,
        required:true

    },

    provinceId:{
        type:String,
        maxlength: 100,
        minlength: 6,
        required:true
    }
});

const Dist = mongoose.model('dist',distSchema);

function validateDist(dist){
    const schema ={
       distName: Joi.string().max(255).min(3).required(),
       distEmail: Joi.string().max(20).min(10).required(),
       provinceId: Joi.string().max(100).min(6).required()

    }
return Joi.validate(dist,schema);
}

module.exports.Dist = Dist;
module.exports.validateDist=validateDist;