const mongoose = require("mongoose");
const express = require("express");
const _=require("lodash");
const router = express.Router();
const {Province,validateProvince}=require('../models/province.model');
const {Dist,ValidateDist}=require('../models/district.model');
const admin=require('../middlewares/admin')

router.post('/',admin,(req, res) => {
    insertNewDist(req, res);
});

router.get('/', (req, res) => {
    Dist.find()
        .then(s => res.send(s))
        .catch(err => res.send(err).send(400));
});

router.put('/',admin,(req, res) => {
    updateIntoMongoDB(req, res);
});


router.delete('/:id',admin,(req, res) => {
    Dist.findByIdAndRemove(req.params.id)
        .then(sch => res.send(sch))
        .catch(err => res.send(err).status(404));
});

function updateIntoMongoDB(req, res) {
    Dist.findOneAndUpdate({ _id: req.body._id },
        req.body, { new: true })
        .then(course => res.send(course))
        .catch(err => res.send(err).status(400));
}

function insertNewDist(req,res){
    const {error} = ValidateDist(req.body);
    if(error) return res.send(error.details[0].message).status(400)
    
    let checkid=Province.find({_id:req.body.email})

    const dist = new Dist(_.pick(req.body,['distName','distEmail','provinceId']));
    if(req.user.provinceId ==  req.body.provinceId){
        dist.save()
        .then(s => res.send(s).status(201))
        .catch(err => res.send(err).send(400));
    }else{
        return res.send("You are not allowed to insert new district")
    }
}

module.exports = router;

