const mongoose=require("mongoose");
const Joi = require('joi')
const bcrypt = require('bcrypt')
const jwt  =  require('jsonwebtoken')
const config = require('config')
Joi.objectId = require('joi-objectid')(Joi)

const userSchema = new mongoose.Schema({
    userName:{
        type:String,
        maxlength: 255,
        minlength:3,
        required:true
    },
    userEmail:{
        type:String,
        maxlength:20,
        minlength:10,
        required:true

    },
    countryId:{
        type:String,
        maxlength: 100,
        minlength: 6
    },

    userPassword:{
        type:String,
        maxlength:100,
        minlength:3,
        required:true
    },

    isAdmin:{
        type:Boolean,
        default:false,
        required:true
    }
});
userSchema.methods.generateAuthToken = function(){
    const token = jwt.sign({_id:this._id,userName:this.userName,countryId:this.countryId,userEmail:this.userEmail,isAdmin:this.isAdmin},config.get('jwtPrivateKey'));
    return token;
}

const User = mongoose.model('user',userSchema);

function validateUser(user){
    const schema ={
        userName: Joi.string().max(255).min(3).required(),
        userEmail: Joi.string().max(20).min(10).required(),
        countryId: Joi.string().max(100).min(6),
        userPassword: Joi.string().max(100).min(3).required(),
        isAdmin: Joi.required()

    }
return Joi.validate(user,schema);
}

module.exports.User = User;
module.exports.validate= validateUser;