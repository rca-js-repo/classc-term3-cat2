const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/schoolMis', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then(() => console.log('connected to mongodb successfully....'))
.catch(err =>console.log('Failed to connect to mongodb!',err));
 
//Connecting Node and MongoDB
require('./country.model')
require('./province.model')
require('./district.model')
require('./sector.model')
require('./user.model')