const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const mongoose=require('mongoose');

const countrySchema = new mongoose.Schema({
    countryName:{
        type:String,
        maxlength: 255,
        minlength:3,
        required:true
    },

    countryEmail:{
        type:String,
        maxlength:20,
        minlength:10,
        required:true
    }

});

const Country = mongoose.model('country',countrySchema);

function validateCountry(country){
    const schema ={
       countryName: Joi.string().max(255).min(3).required(),
       countryEmail: Joi.string().max(20).min(10).required()

    }
return Joi.validate(country,schema);
}

module.exports.Country = Country;
module.exports.validateCountry=validateCountry;