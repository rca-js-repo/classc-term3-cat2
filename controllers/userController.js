const hashPassword = require('../utils/hash')
const _= require('lodash')
const express = require('express');
const bcrypt=require('bcrypt');
const {User,validate} =  require('../models/user.model')
const admin=require('../middlewares/admin')

var router = express.Router();

router.post('/',async (req,res) =>{
    const {error} = validate(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    let user  = await User.findOne({userEmail:req.body.userEmail})
    if(user) return res.send('User already registered').status(400)

     /* user  =  new User({
        name:req.body.name,
        email : req.body.email,
        password : req.body.password
    }); */
   user  =  new User(_.pick(req.body, ['userName','userEmail','userPassword']))
    const hashed = await hashPassword(user.userPassword)
   user.userPassword = hashed
    await user.save()
    //return res.send(user).status(201)
    return res.send(_.pick(user,['_id','userName','userEmail'])).status(201)
});

module.exports = router;


// router.get('/',async (req,res)=>{
//     const users = await User.find().sort({name:1});
//     return res.send(users)
// });

// router.post('/',async (req,res) =>{
//     const {error} = validate(req.body)
//     if(error) return res.send(error.details[0].message).status(400)

//     //check if the user exits in the db
//     let user  = await User.findOne({email:req.body.email})
//     if(user) return res.send('User already registered').status(400)

//     user  =  new User(_.pick(req.body, ['userName','userEmail','countryId','password','isAdmin']))
//     const hashed = await hashPassword(user.password)
//    user.password = hashed
//    await user.save();
//     return res.send(_.pick(user,['_id','userName','userEmail','countryId','isAdmin'])).status(201)
// });

// router.post('/login',async (req,res) =>{
//   let user  = await User.findOne({email:req.body.email})
//   if(!user) return res.send('Invalid email or password').status(400)
//   const validPassword = await bcrypt.compare(req.body.password,user.password)
//   if(!validPassword) return res.send('invalid email or password').status(400)
//  //const token  =jwt.sign({_id:user._id,name:user.name},config.get('jwtPrivateKey'))
//   //return res.send(token);
//   return res.send(user.generateAuthToken());
// });

// module.exports = router;