const mongoose = require("mongoose");
const express = require("express");
const _=require("lodash");
const router = express.Router();
const {Province,validateProvince}=require('../models/province.model');
const {Country,ValidateCountry}=require('../models/country.model');
const admin=require('../middlewares/admin')

router.post('/',admin,(req, res) => {
    insertNewProvince(req, res);
});

router.get('/', (req, res) => {
    Province.find()
        .then(s => res.send(s))
        .catch(err => res.send(err).send(400));
});

router.put('/',admin,(req, res) => {
    updateIntoMongoDB(req, res);
});

router.delete('/:id',admin,(req, res) => {
    Province.findByIdAndRemove(req.params.id)
        .then(sch => res.send(sch))
        .catch(err => res.send(err).status(404));
});

function updateIntoMongoDB(req, res) {
    Province.findOneAndUpdate({ _id: req.body._id },
        req.body, { new: true })
        .then(course => res.send(course))
        .catch(err => res.send(err).status(400));
}

function insertNewProvince(req,res){
    const {error} = validateProvince(req.body);
    if(error) return res.send(error.details[0].message).status(400)
    
    let checkid=Country.find({_id:req.body.email})

    const province = new Province(_.pick(req.body,['provinceName','provinceEmail','countryId']));
    if(req.user.countryId ==  req.body.countryId){
        province.save()
        .then(s => res.send(s).status(201))
        .catch(err => res.send(err).send(400));
    }else{
        return res.send("You are not allowed to insert new province")
    }
}

module.exports = router;