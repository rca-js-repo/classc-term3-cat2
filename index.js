require("./models/mongodb");
const express=require("express")
const app=express();
const countryController=require('./controllers/countryContoller');
const provinceController=require('./controllers/provinceConntoller');
const distController = require('./controllers/districtController');
const sectController = require('./controllers/sectorContoller');
const userController=require('./controllers/userController');
const config = require('config')
const admin=require('./middlewares/admin');
const authMiddleware = require('./middlewares/auth')
const bodyparser = require('body-parser');
const auth = require('./controllers/auth');

app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

if(!config.get("jwtPrivateKey")){
    console.log('JWT PRIVATE KEY IS NOT DEFINED')
    process.exit(1)
}

app.get('/',(req,res)=>{
    res.send("Welcome to our Country Management System");
});


app.use('/api/countries', authMiddleware, countryController);
app.use('/api/provinces', authMiddleware, provinceController);
app.use('/api/dists', authMiddleware, distController);
app.use('/api/sect', authMiddleware, sectController);
app.use('/api/users',userController)//signup
app.use('/api/auth',auth)//jwt login

const port = 2186
app.listen(port,function(){
    console.log(`Listening to port ${port}...`);
})
 
