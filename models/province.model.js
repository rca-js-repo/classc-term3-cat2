const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const mongoose=require('mongoose');

const provinceSchema = new mongoose.Schema({

    provinceName: {
        type:String,
        maxlength: 255,
        minlength:3,
        required:true
    },

    provinceEmail: {
        type:String,
        maxlength:20,
        minlength:10,
        required:true
    },

    countryId:{
        type:String,
        maxlength: 100,
        minlength: 6
    }
});


const Province = mongoose.model('province',provinceSchema);

function validateProvince(province){
    const schema ={
       provinceName: Joi.string().max(255).min(3).required(),
       provinceEmail: Joi.string().max(20).min(10).required(),
       countryId: Joi.string().max(100).min(6)

    }
return Joi.validate(province,schema);
}

module.exports.Province = Province;
module.exports.validateProvince=validateProvince;