const mongoose=require("mongoose");
const express=require("express");
const _=require("lodash");
const router=express.Router();
const{Country,validateCountry} = require('../models/country.model');
const admin = require('../middlewares/admin');

router.post('/',admin,(req,res)=>{
    insertNewCountry();
});

router.get('/',(req,res)=>{
    Country.find()
    .then(s=>res.send(s))
    .catch(err=>res.send(err).send(400));
});

router.delete('/:id',admin,(req, res) => {
    Country.findByIdAndRemove(req.params.id)
        .then(sch => res.send(sch))
        .catch(err => res.send(err).status(404));
    });

router.put('/',admin,(req,res)=>{
    Country.findByIdAndUpdate({_id:req.body._id})
       .then(country=>res.send(country))
       .catch(err=>res.send(err));
});

function insertNewCountry(req,res){
    const{error} = validateCountry(req.body);
    if(error) return res.send(error.details[0].message).status(400);

    let country = new Country(_.pick(req.body,["countryName","countryEmail"]));
    country.save()
    .then(s=>res.send(s).status(201))
    .catch(err=>res.send(err).send(400));
}

module.exports=router;


