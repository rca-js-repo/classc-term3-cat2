const mongoose = require("mongoose");
const express = require("express");
const _=require("lodash");
const router = express.Router();
const {Sect,validateSect}=require('../models/sector.model');
const {Dist,ValidateDist}=require('../models/district.model');
const admin=require('../middlewares/admin')

router.post('/',admin,(req, res) => {
    insertNewSect(req, res);
});

router.get('/', (req, res) => {
    Sect.find()
        .then(s => res.send(s))
        .catch(err => res.send(err).send(400));
});

router.put('/',admin,(req, res) => {
    updateIntoMongoDB(req, res);
});

router.delete('/:id',admin,(req, res) => {
    Sect.findByIdAndRemove(req.params.id)
        .then(sch => res.send(sch))
        .catch(err => res.send(err).status(404));
});

function updateIntoMongoDB(req, res) {
    Sect.findOneAndUpdate({ _id: req.body._id },
        req.body, { new: true })
        .then(course => res.send(course))
        .catch(err => res.send(err).status(400));
}

function insertNewSect(req,res){
    const {error} = validateSect(req.body);
    if(error) return res.send(error.details[0].message).status(400)
    
    let checkid=Dist.find({_id:req.body.email})

    const sect = new Sect(_.pick(req.body,['sectName','sectEmail','distId']));
    if(req.user.distId ==  req.body.distId){
        sect.save()
        .then(s => res.send(s).status(201))
        .catch(err => res.send(err).send(400));
    }else{
        return res.send("You are not allowed to insert new sector")
    }
}

module.exports = router;