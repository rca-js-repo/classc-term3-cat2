const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const mongoose=require('mongoose');

const sectSchema = new mongoose.Schema({
    sectName:{
        type:String,
        maxlength: 255,
        minlength:3,
        required:true
    },

    sectEmail:{
        type:String,
        maxlength:20,
        minlength:10,
        required:true
    },

    distId:{
        type:String,
        maxlength: 100,
        minlength: 6,
        required:true
    }
});

const Sect = mongoose.model('sect',sectSchema);

function validateSect(sect){
    const schema ={
        sectName:  Joi.string().max(255).min(3).required(),
        sectEmail:  Joi.string().max(20).min(10).required(),
        distId: Joi.string().max(100).min(6).required()
    }

    return Joi.validate(sect,schema);
}

module.exports.Sect = Sect;
module.exports.validateSect = validateSect;
